# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import requests
import os
from datetime import datetime

resTodos = requests.get('https://json.medrating.org/todos')
resUsers = requests.get('https://json.medrating.org/users')
jUsers = resUsers.json()
jTodos = resTodos.json()
path = os.getcwd()


def createFolderInDirectory():
    pathFolder = path + '/tasks'
    try:
        os.mkdir(pathFolder)
    except OSError:
        print("Создать директорию %s не удалось" % pathFolder)
    else:
        print("Успешно создана директория %s " % pathFolder)


def createFile(self, i):
    counterTrue = 0
    counterFalse = 0
    listTrue = ''
    listFalse = ''
    j = 0
    try:
        while j < len(jTodos):

            try:
                if jTodos[j]['userId'] == i:
                    if jTodos[j]['completed']:
                        counterTrue += 1
                        if len(jTodos[j]['title']) > 48:
                            d = jTodos[j]['title']
                            listTrue += d[:48] + '...' + '\n'
                        else:
                            listTrue += jTodos[j]['title'] + '\n'
                    else:
                        counterFalse += 1
                        if len(jTodos[j]['title']) > 48:
                            d = jTodos[j]['title']
                            listFalse += d[:48] + '...' + '\n'
                        else:
                            listFalse += jTodos[j]['title'] + '\n'
            except KeyError:
                print()
            j += 1

        dataDoc = datetime.now().strftime("%Y.%m.%d %H:%M")
        fullTasks = counterFalse + counterTrue
        f = open(path + '/tasks/' + 'buff.txt', 'w')
        f.writelines('Отчёт для ' + jUsers[i]['company']['name'] + '.' + '\n' +
                     jUsers[i]['name'] + ' <' + jUsers[i]['email'] + '> ' + str(dataDoc) + '\n' +
                     'Всего задач: ' + str(fullTasks) + '\n\n' +
                     'Завершенные задачи (' + str(counterTrue) + '):' + '\n' +
                     listTrue + '\n\n' +
                     'Оставшиеся задачи (' + str(counterFalse) + '):' + '\n' +
                     listFalse)
    except:
        os.remove(path + '/tasks/' + 'buff.txt')
        print('ошибка при записи файла')


def workingWithFiles():
    createFolderInDirectory()
    i = 0
    while i != len(jUsers):
        createFile(0, i)
        pathFile = path + '/tasks/' + jUsers[i]['username']

        try:

            os.rename(path + '/tasks/buff.txt', pathFile + '.txt')

        except FileExistsError:
            current_datetime = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
            os.rename(pathFile + '.txt',
                      path + '/tasks/' + 'old_' + jUsers[i]['username'] + '_' + str(current_datetime) + '.txt')
            os.rename(path + '/tasks/buff.txt', pathFile + '.txt')
        i += 1


workingWithFiles()  # start
